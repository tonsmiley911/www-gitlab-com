---
layout: markdown_page
title: "Product Section Vision - Dev"
---

- TOC
{:toc}

![Dev Overview](/images/direction/dev/dev-overview.png)

## Dev Section Overview

The Dev Section is made up of the [Manage](/handbook/product/categories/#manage-stage), [Plan](https://about.gitlab.com/direction/plan/index.html), and [Create](/handbook/product/categories/#create-stage) stages of the DevOps lifecycle. These stages mark the leftmost side of the DevOps lifecycle and primarily focus on the creation and development of software. The scope for Dev stages is wide and encompasses a number of analyst categories including Value Stream Management, Project Portfolio Management, Enterprise Agile Planning Tools, Source Code Management, IDEs, Design Management, and even ITSM. It is difficult to truly estimate TAM for the Dev Section, as our scope includes so many components from various industries, but research indicates the estimated [TAM](https://docs.google.com/spreadsheets/d/1HYi_l8v-wTE5-BUq_U29mm5aWNxnqjv5vltXdT4XllU/edit?usp=sharing) in 2019 is roughly ~$3B, growing to ~$7.5B in 2023 (26.5% CAGR).

Based on [DevOps tool revenue](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) at the end of 2018 and comparing to GitLab annual recurring revenue at the end of FY20 Q3, our estimated market share is approximately 1.5% based on revenue. (Note: this assumes we can attribute 100% of GitLab revenue to Dev stages.) Market share based on source code management would most likely range far higher.

Nearly [half of organizations](https://drive.google.com/file/d/17ZSI2hGg3RK168KHktFOiyyzRA93uMbd/view?usp=sharing) still have not adopted DevOps methodologies, despite [data](https://drive.google.com/file/d/17MNecg84AepxWlSDB5HjNBrCJggaS9tP/view?usp=sharing) that indicates far higher revenue growth for organizations that do so. Migrating a code base to a modern, Git-backed source control platform like GitLab can often be the first step in a DevOps transformation. As such, we must provide industry-leading solutions in source code and code review, as this is not only the entry into DevOps for our customers, but typically the entry into the GitLab platform. Once a user has begun utilizing repositories and code review features like Merge Requests, they often move “left” and “right” to explore and utilize other capabilities in GitLab, such as CI and project management features.

Per our [Stage Monthly Active User Data](https://app.periscopedata.com/app/gitlab/425984/Month-over-Month-Overestimated-SMAU), Manage and Create have the highest usage amongst GitLab stages. As such, these stages must focus on security fixes, bug fixes, performance improvements, UX improvements, and depth more than other areas of GitLab. Plan, while introduced in 2011 with the release of issue tracking, still falls far behind market leaders who have better experiences for sprint management, portfolio management, roadmapping, and workflows.

Other areas, such as Value Stream Management are nascent to both GitLab and the market, and will require more time devoted to executing problem and solution validation discovery. 

Over the next year, Dev will require focus on both breadth and depth activities, and each stage will require significant investment to accelerate the delivery of security issues, performance issues, and direction items. 

## Resourcing and Investment
The existing team members for the Dev section can be found in the links below:
* [Development](https://about.gitlab.com/company/team/?department=dev-section)
* [UX](https://about.gitlab.com/company/team/?department=dev-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=dev-pm-team)
* (https://about.gitlab.com/company/team/?department=dev-qe-team)

## Dev Section SWOT Analysis & Challenges

### Strengths

* End-to-end platform with seamless connections to downstream sections like CI/CD, Ops, Secure, and Defend.
* Strong analyst relationships allow us to help define nascent markets like Value Stream Management.
* Exceptionally talented product team with deep industry expertise. 
* [Open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) with both self-managed and SaaS options.
* Expansive feature set and roadmap that aligns to validated market needs. 
* We’ve chosen to invest in a storage architecture ([Gitaly](https://about.gitlab.com/blog/2018/09/12/the-road-to-gitaly-1-0/)) that will lead to future performance enhancements and high-availability options.

### Weaknesses

* Our SaaS product (GitLab.com) is not yet enterprise grade, and several improvements are needed for enterprise adoption of self-managed instances.
* Performance issues continue to mount in various areas (ex: MR load times, diff rendering, Web IDE load times) as we add new features and code.
* Some areas of Dev are not competitive; for example Wikis, Snippets, and Roadmaps.
* Our company name may be limiting our opportunities for the Plan stage by creating the sense that the features only work for the software development lifecycle. We may need to consider changing how we package capabilities for customer segments who desire to use GitLab for project or portfolio management.

### Opportunities

* Consideration of new business models/licensing plans could lead to additional adoption of features.
* Take a leadership position in Value Stream Management, a nascent market with many vendors who aren’t doing it well.
* Big opportunity to grow and/or shift usage to SaaS, allowing customers to receive value more quickly from GitLab without provisioning and maintaining their own instance.
* Disrupting dominant legacy designs by creating a modern, lovable system for requirements management, workflows, and permissions.
* Creating an “easy migration” path from other popular DevOps tools like Jenkins and GitHub would expedite our market share growth.
* Increasing investment into Wikis, Gitaly, Compliance, and Boards will provide more focus to these areas, resulting in additional IACV.

### Threats

* Onboarding new team members may lead to a slow down in velocity if not managed carefully.
* Other companies, such as Microsoft, may begin to innovate more quickly than they have in previous years, closing the cracked window of opportunity for us to gain market share.
* If we stop talking to customers to better understand their problems and pain points, we will continue to ship code and features at a high velocity, but the things we ship may not add value for our customers. This is known as the [build trap](https://melissaperri.com/blog/2014/08/05/the-build-trap).
* Dev tools start to become commoditized, with IaaS providers giving them away for free with IaaS/cloud services contracts.
* The Dev section currently has the highest volume of high-priority defects and security issues, which will naturally impact the ability to ship direction items as quickly as expected. This could result in slower business growth, as direction items are typically tied to IACV or customer upsell/retention opportunities.

## Vision Themes
Our vision for the Dev section is to provide the world’s best product creation platform. We believe we have a massive opportunity to change how cross-functional, multi-level teams collaborate by providng an experience that breaks down organizational silos and enables better collaboration. We want to deliver a solution that enables higher-quality products to be created faster. The following themes are listed below to surface our view of what will be important to the market and to GitLab over the next 3 to 5 years. As such, they will be the cornerstone of our 3-year strategy, and all activities in the 1-year plan should advance GitLab in one or more of these areas.

### Efficient and Automated Code Review
Code review should be a delightful experience for all involved in the process. Over time, we expect the code review process to evolve from where it is today to become a mostly automated process in the future. Along the way, incremental improvements will occur, where developer platforms like GitLab will focus on performance and usability of the code review tools. Code review should be an efficient process, and the easier GitLab can make code review, the more efficient dev teams become. Although unproven, better code review should reduce the number of bugs and increase the amount of higher-quality features an organization can ship. The code review process will continue to provide a venue for developers to learn and collaborate together.

As examples, GitLab will:
* Load large, multi-file diffs faster than any other comparable product on the market.
* Provide tailored insights to the code reviewer, alerting them to the most important areas to review.
* Allow for client- and server-side evaluation of code where possible, and integrate it into the code review process.

### Measurement and Increased Efficiency of the Value Stream
Peter Drucker has stated “If you can’t measure it, you can’t improve it.” Many software development teams have no way of measuring their efficiency, and even if they do, there is not enough feedback, information, or actionable insights to improve the efficiency of their team. Even then, once efficiency is improved, it can be difficult to tell if a team’s performance is good or bad, as there is often no point of comparison. Even the best performing team in an organization could be worse than the competition. Increasing efficiency is paramount to companies increasing their **time to value** and helping organizations answer **“Is my DevOps transformation working?”**

We believe efficiency can be improved in two ways. The first way is improving existing value stream activities and making them more efficient. This focuses on making existing activities as fast as possible. The second way is to question and change the value stream into higher value-added activities at each step. GitLab’s vision is to help answer both of these questions: “Am I doing things fast enough?” and “Am I doing the right things?”

Today, value stream management is largely focused on visualizing the value chain through deployment. GitLab is uniquely positioned to also visualize, track, and measure value chain activities to the right of deployment. For example, the value created by post launch activities, such as press releases, blog posts, and marketing campaigns should funnel into value stream management, while providing the business the right data and insights for their value chain.

As examples, GitLab will provide:
* Easy-to-use and customizable tools that measure the efficiency of the DevOps lifecycle.
* Insight into areas of waste where teams can improve.
* Recommendations based on large data sets of other teams using GitLab for comparison.
* A visual experience for value stream management that goes beyond code deployment.

### DevOps for More Personas
DevOps started with the merging of Development and Operations and has since been augmented to include Security in some circles, highlighting DevSecOps as the next trend. There are many other personas that are involved in software development, such as product managers, project managers, product designers, finance, marketing, procurement, etc. These personas will continue to expand until nearly every role at knowledge-work companies touches some facet of the DevOps lifecycle. Over time, organizations will realize that teams who work out of the same platform/set of tools are more efficient and deliver faster business and customer value.

Because of this trend, each persona of the DevOps lifecycle should ultimately be treated as a first-class citizen in GitLab. 

As examples, GitLab will provide:
* A better experience for project management workflows.
* A space for product designers to design and collaborate on designs with product managers and engineers.
* A Web IDE experience that is able to run the GDK, serving collaborators of all skill sets and hardware, allowing them to contribute to GitLab.

### Enterprise Digital Transformation
While we will continue to solve for the [modern DevOps use case first](https://about.gitlab.com/handbook/product/#modern-first), most enterprise customers have custom requirements that GitLab does not solve for today. This is a wide-ranging set of custom controls that spans systems such as permissions, approvals, compliance, governance, workflows, and requirements mapping. It is our belief these needs will exist for many years to come, and we will need to incorporate these to truly become a flexible DevOps platform that serves enterprise segments. We will strive to do this in ways that are modern and, where possible, adhere to a [“convention over configuration”](https://about.gitlab.com/handbook/product/#convention-over-configuration) approach, living with the cognitive dissonance that sometimes flexibility will be required in areas we have not been willing to venture into thus far.

Additionally, compliance, auditing, and surfacing evidence of security/compliance posture will become more important as more GDPR-like legislation is enacted and passed into law. GitLab should make it easy to not only surface and deliver evidence for GitLab controls (i.e. who has access to GitLab, who did what on what group, etc.), but also to track and manage compliance requirements for various legislation our customers may be bound to.

As examples, GitLab will provide:
* Customizable workflows, unlocking enforcement, approvals, and insight into these workflows.
* More customizable and fine-grained permissions.
* Logs for everything that’s done within GitLab and allow those events to be accessible via the API and UI.
* Alerting on GitLab audit events.

### Project Management Morphs into Product Management
Product Managers often struggle with answering the question, "Is the product or feature I just launched successful?" There are many sensing mechanisms to help answer this question, including revenue, users, customer feedback, NPS, etc., but no product currently helps product managers exhaustively manage the product development lifecycle from end to end. Many products assist with planning, delivery of code, and deployment, but feedback and iteration are equally as important to product managers as shipping the first iteration. Getting the first iteration out is traditionally celebrated, but is only one of many steps to true product development lifecycle management.

Imagine an experience where product managers can log in and view the "health" of their entire portfolio on one dashboard. It is clear which features have the most value to customers (and by extension to the business) as measured by key metrics, assisting PMs with priortization activities. PMs can quickly identify features or products within their portfolio that need more attention and drill into them, identifying the correct next action to take, whether it's iteration on the feature or perhaps sunsetting it. PMs can quickly create an issue for the next iteration, version control features, view security incidents, respond to customer feedback, drill down into analytics, control A/B tests of the feature, and even interact with users of the feature or product directly by creating ad-hoc surveys or questions for users to answer. Additionally, the experience should allow for ROI analysis and tracking of the ROI after capital has been expended.

Within three years, project management tools will begin evolving to provide this experience and help PMs answer tough product questions. These tools will also assist with measuring and predicting value to the organization, if a specific action is prioritized by the PM. The ideal solution most likely uses data science and predictive analytics to assist product managers with decisions both before and after a feature is launched.

As examples, GitLab will provide:
* Feature management capabilities, including the ability to manage a feature as an object inside of GitLab that lives on after an issue is closed.
* An experience where PMs can quickly analyze the health of all relevant features.
* A framework that helps PMs with prioritization decisions.
* A framework for ROI analysis and measurement.

## 3-Year Strategy

In three years, the Dev Section market will:
* Centralize around Git as the version control of choice for not only code, but for design assets, gaming, silicon designs, and AI/ML models.
* Have a market leader emerge in the value stream management space. Currently, the market is fragmented with most players focused on integrations into various DevOps tools.
* Adopt a mindset shift from project management to product management.
* Recognize the value of a single platform for all software creation activities, including product management.

As a result, in three years, GitLab will:
* Provide a next-generation, highly performant Git-backed version control system for large assets, such as ML models. Our goal in three years should be to host the most repositories of these non-code assets.
* Emerge as the leader in VSM and be recognized in the industry by customers and analysts as such. Our goal in three years should be to provide the best insights into the product development process that no other tool can come close to, as we have a [unified data model](https://www.ca.com/en/blog-itom/what-is-a-unified-data-model-and-why-would-you-use-it.html) due to GitLab being a single platform.
* Develop an industry-leading product management platform where multiple features and products can be measured and managed easily.

## 1-Year Plan: What’s Next for Dev

Over the next 12 months, each stage in the Dev section will play an integral part in this strategy.

Please see the [categories page](https://about.gitlab.com/handbook/product/categories/#dev-section) for a more detailed look at Dev's plan by exploring `Strategy` links in areas of interest.

### Manage

For administrators and executives, the process of management is always on. It extends to managing people, money, and risk; when the stakes are high, these stakeholders demand an experience and feature set that makes them feel in control. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to make software work for them.

Not only do we want to fulfill those fundamental needs, we want to give you the freedom to work in new and powerful ways. We aspire to answer questions managers didn't know they had, and to automate away the mundane.

For these users, Manage's role in GitLab is to serve across stages to **help organizations prosper with configuration and analytics that enables them to work more efficiently**. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

* Manage Overview: [See video on YouTube](https://youtu.be/uzzbuZ4C9q4)
  * Compliance Group Overview and Roadmap: [See video on YouTube](https://youtu.be/c3aMpOeXH28)
  
#### North Stars

**Enterprise readiness:** GitLab must be seen as a platform that enterprises can use out of the box for both GitLab.com and self-managed deployments. We're doing this by focusing on improvements in several key areas:
  * Enterprise-grade authentication and authorization. Critical for large organizations managing users at scale, we're focused on investing in SAML SSO that works across a range of identity providers and automates member management.
  * Improving tools that help compliance-minded organizations thrive. GitLab makes it easy to contribute, but administrators should have comprehensive and consistent views on instance activity. We'll improve [audit management](https://about.gitlab.com/direction/manage/audit-management/) to a lovable category and introduce dashboarding and alerting to help tell your compliance story to stakeholders. We'll also solve a pronounced need for fine-grained member permissions.

**Lowering time to production for our customers:** Improvements to productivity and code analytics over the next 12 months will allow our customers to drill down and identify sources of waste in their existing process. Within 12 months, GitLab customers will be able to firmly answer how much their time-to-production metrics have improved.

**A great import experience:** Few instances start from scratch - for most, one of the earliest tasks for a GitLab administrator is importing information from outside the application. We'll invest heavily in a strong import user experience and build bespoke importers for key competitors like Jenkins and Jira. We'll also expand on the capabilities of our existing importers, with a focus on making GitLab.com migration easy.

### Plan

**Kanban Boards**: Current project management tools are capable, but suffer from usability. Trello made significant gains by focusing on the user experience. Unfortunately, Trello chose to be a general tool which left some software teams wanting features designed specifically to help with software development and delivery. GitLab has an opportunity to re-design Kanban boards for software teams; think of how Jira could work if it were designed by Trello as opposed to the other way around. Our boards need to evolve to be a primary interface, a complete WYSIWYG document view where everyone who is looking on board X is seeing the same thing (updated in realtime), with rich interaction without having to leave the board. This may include changes such as having short summaries, first-class checklists, quick filters, etc. In addition, boards need to focus on common workflows of software teams such as issue triage, daily workflow, sprint planning, quarterly planning, executive reporting, etc.

**Importing from JIRA without losing required data**: In the next 12 months, we will deliver enforced workflows, a better roadmap experience, cumulative flow diagrams, and improvements to boards in order to enable a better planning and project management experience.

**Enhancing Portfolio and Project Roadmaps**: Provide easy-to-use, cross-team roadmaps at the portfolio, project, and epic level that allow users across the organization to see how work is progressing and identify dependencies and blockers. Organize and prioritize work though dynamic roadmaps in real time. 

**Easy Top-Down Planning**: Enhanced portfolio management experience allowing customers to start planning from the top; creating initiatives, projects, and epics while laying them out on a roadmap prior to issues and milestones being created. Provide analytics at each level, and allow linking of each object to provide deeper dependency mapping across multiple teams and projects. Enable users to create strategic initiatives and assign work, impact, and resources to each to help them make the right business decisions. Additionally, in order for our users to get more value out of Plan, we will be implementing [Epic features to be more aligned with our buying tiers](https://gitlab.com/groups/gitlab-org/-/epics/1887).

**Reporting and Analytics**: Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio.

**Requirements Management**: Many regulated customers desire to use GitLab for requirements mapping, dependencies, and process management. GitLab will provide these capabilites in a modern-first way.

### Create

**Realtime:** It's time to fully embrace realtime. Many parts of GitLab update in near realtime, but not everything does, and unfortunately some of the parts thare are left are critical to a great experience. Realtime kanban boards is mentioned above in Plan, but within Create, there's tons of opportunity.

**Enhancing the code review experience:** In the next 12 months, we must focus code review to be more performant and intelligent. We will do this by investing in [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/1417), adding additional code review functionality such as jump to definition, identifying references, displaying function documentation and type signatures, and adding support for first-class reviewers. Code review should be an "IDE like" experience.

**Making large files “just work” in Git:** To gather more market share from industries that currently use Perforce or SVN, we must invest in making the large-file experience in Git excellent. It should “just work” without configuration or specialized hardware.

**Investing in our Wiki product:** Many customers currently use Wikis for knowledge bases and project management activities. Our first step in making the GitLab Wiki more competitive is making wikis available at the group level, enhancing markdown support, and creating a [WYSIWYG experience](https://en.wikipedia.org/wiki/WYSIWYG).

**Enabling easier contributions to GitLab:** Contributing to GitLab requires users to set up and run the [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit) locally. This is cumbersome and typically requires multiple hours of debugging with senior engineers. While the process of streamlining the GDK locally should be advanced, GitLab should also provide the GDK as part of the Web IDE experience. Allowing contributors to quickly spin up feature branches should encourage more contributions from non-engineering GitLab team members, as well as the wider community.

**Bolstering the editor experience:** Our current Web IDE experience is useful for small changes, but has not proven itself useful as an actual replacement for a local IDE. Over the next year, we will evaluate the impact of adding a container-based IDE solution, while continuing to streamline our editing experience, potentially by sunsetting the ACE editor. Additionally, we will improve the IDE experience with self-hosted, client-side evaluation, server-side evaluation, and live-coding features for pair programming.

### What we're not doing

Choosing to invest in these areas in 2020 means we will choose not to:
* Invest in features that help companies answer, “Am I doing the right activities?” Answering this question is something we will focus on in years two and three of the VSM plan.
* Treat ML models as first-class citizens in GitLab. Instead, we will focus on getting large assets to become performant via improvements to Gitaly. Once this is completed, we will focus on ML models.
* Provide recommendations where customers can improve their efficiency in the DevOps lifecycle. This will likely require comparisons amongst many GitLab users and an AI engine to make intelligent recommendations. These improvements will come in years two and three of the VSM plan.

### Other areas of investment consideration
* Data science: We should consider investment into a data science team that can assist with recommendations for Plan and VSM features.
* [Dark themes](https://gitlab.com/gitlab-org/gitlab-ee/issues/14531): We should consider prioritizing a dark theme for both GitLab, as well as the Web IDE/editing experience. This is an expected feature of most modern development tools.
* Engineering: Most Dev groups should see 50-100% headcount growth in order to make our Dev categories lovable.
* AI: We should consider beginning to invest into AI as a solution for recommendations; for example, recommended assignees, labels, etc.

<%= direction["all"]["all"] %>
