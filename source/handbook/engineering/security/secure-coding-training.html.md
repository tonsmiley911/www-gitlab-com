---
layout: markdown_page
title: "GitLab Security Secure Coding Training"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# GitLab Security Secure Coding Training

This page contains information on secure training initiatives sponsored by the GitLab Security team.

## Secure Coding Guidelines

These guidelines cover how to address specific classes of vulnerabilities that have been identified in GitLab.

- [XSS](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/xss.md)
- [SSRF](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/ssrf.md)
- [Permissions](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/permissions.md)
- [Regular Expressions](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/regex.md)

## Secure Coding Training with Jim Manico

### Description

A developer focused application security training presented by Jim Manico, and Dr. Justin Collins, the creator of Brakeman, occurred on the days of July 29th and 30th 2019.

You can find the recorded, private YouTube stream at the following:

- [Day 1 Morning](https://www.youtube.com/watch?v=PXR8PTojHmc)
- [Day 1 Afternoon](https://www.youtube.com/watch?v=2VFavqfDS6w)
- [Day 2 Morning](https://www.youtube.com/watch?v=bJYUxKn88so )
- [Day 2 Afternoon](https://www.youtube.com/watch?v=8tP2KVKHO7A)

### Schedule and Topics

#### Day 1

- Morning
  1. Introduction to Application Security
  1. Threat Modeling
  1. OWASP Top Ten 2017
  1. OWASP ASVS 4.0
  1. Multi-Form Workflow Security

- Afternoon
  1. DevOps Best Practices ​
  1. XSS Defense ​Rails (HAML)
  1. Coding Vue.js applications securely
  1. File Upload and File IO Security ​Multi-Step Secure File Upload Defense, File I/O Security
  1. Input Validation ​Basics ​(Whitelist Validation​ and Safe Redirects)
  1. 3rd Party Library Security Management ​(​Detect and manage insecure 3rd party libraries)

#### Day 2

- Morning
  1. Dynamic render paths and local file inclusion
  1. IDOR and scoped queries
  1. SSRF Defense
  1. Cross Site Request Forgery CSRF Defenses for multiple architecture types (stateless, API,web, etc)

- Afternoon
  1. Authentication Best Practices
  1. Introduction to the OAuth authorization protocol
  1. Secure Secret Storage
  1. Encrypted secrets/credentials
  1. Open Q/A and Misc Topics

### Additional resources

- [PowerPoint presentations](https://drive.google.com/drive/folders/1NRrlnqwkhsS-UmuagwoD8GB4APXsfJxb?usp=sharing)
- [Burp Proxy](https://portswigger.net/burp/communitydownload)
- [Online Labs](https://manicode.us/shepherd/)
- [Questions Doc](https://docs.google.com/document/d/1KsK5DBDgiF8k0N3cs89o1VsMYsUWUPH9fIQb_smFEac/edit)
