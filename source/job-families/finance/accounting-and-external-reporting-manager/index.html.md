---
layout: job_family_page
title: "Accounting and External Reporting Manager"
---

GitLab is looking for an essential member who can manage financial reporting, our external audit, coordination with local service providers for each of our entities and equity accounting with a proven ability to keep up with a fast paced environment. You will join our team in its early stages and be responsible for developing a highly efficient, world class accounting and reporting function. We expect you will know your way around GAAP principles, financial statements and be a proven problem solver.


## Responsibilities

* Primary responsibility to coordinate the external audit and review process conducted by EY. In this capacity you will work closely with the Technical Accounting Manager and Accounting Operations. 
* Primary interface with local service providers for each operating entity to ensure compliance with statutory and tax reporting.  In this capacity you will work closely with the Director of Tax. 
* Efficient, accurate, and timely production of consolidated financial statements; specifically, managing the financial statement preparation process on a monthly, quarterly and annual basis.
* Develop, implement and maintain systems, procedures and policies to support the possibility of a public filing, which would include Form S-1 and the related subsequent SEC reporting requirements including Forms 10-K, 10-Q, 8-K and Proxy filings.
* Assist with recording, reconciling, and analysis of stock based compensation and equity related accounts and ensure the proper accounting and reporting of the same.
* Ensure compliance with Sarbanes-Oxley Section 404 key controls in the financial areas of responsibility.
* Assist with monitoring the need for business process improvements and assist with the design processes, procedures, and reporting enhancements to improve financial and operational processes.
* Initiate design and implementation of new policies and procedures to support new audit requirements and business activities.
* Assist with preparation of reports and presentations for Board of Directors and Audit Committee meetings.
* Respond to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Work with and support the accounting team in day-to-day activities, special projects, and workflow process improvements.
* Support overall department goals and objectives

## Requirements

* Proven work experience as an Accounting Manager or similar leadership role
* Public company accounting experience and SEC reporting experience is required
* Ability to contribute to the career development of staff and a culture of teamwork
* Strong working knowledge of GAAP principles and financial statements
* Must have experience with Netsuite or other big ERP system
* Proficient with excel and google sheets
* International experience preferred
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* Proficiency with GitLab
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).

## Performance Indicators

- [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
- [Number of material audit adjustments = 0](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of ineffective Sox Controls = 0%](/handbook/internal-audit/#internal-audit-performance-measures)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
